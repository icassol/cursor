![](Curso.png)

# Acerca de este curso

Este curso te proporcionará las bases del lenguaje de programación estadística R, 
   uno de los lenguajes de programación más utilizados para la estadística, el cual te permitirá escribir programas que lean, 
   manipulen y analicen datos cuantitativos. Te explicaremos la instalación del lenguaje, fundamentos de programación y  
   también verás una introducción a los sistemas base de gráficos. Los últimos módulos están orientados a la construcción 
de documentos rmarkdown y de aplicaciones web a través del paquete shiny. Además también abordarás la utilización de uno 
   de los IDEs más populares entre la comunidad de usuarios de R, llamado RStudio.
  
# Dr. Ing. Ignacio Cassol

El Dr. Cassol es ingeniero en informática, Master en Software engineering por el Instituto de Buenos Aires (ITBA) y por el Politénico de Madrid. 
     Es doctor en Matemática computacional e industrial (UNICEN) y realizó una estancia post-doctoral como investigador y profesor visitante
     en TECNUN (Escuela de Ingenieros de la Universidad de Navarra). Además de su tarea docente, es Director de Ingeniería Biomédicas de la 
     Facultad de Ingeniería de la Universidad Austral. 
     Sus areas de investigación son le refactorización de código estructurado y la genómica y la ciencia de datos.
     
[Curso 1](https://gitlab.com/icassol/cursor/Mod1.html)
[Curso 2]("/Mod2.html")
[Curso 3]("/Mod3.html")

